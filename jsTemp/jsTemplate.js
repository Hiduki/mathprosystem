/*
MathProSystem Dep. js template
*/
$.fn.fullpage.moveSlideRight(); //右のページへスライド

/*
ラジオボタン，チェックボックスの正誤判定
param : none
必要なid:<form>タグのname,
		ラジオボタン, チェックボックスのname(一つ)
チェックボックスとラジオボタンのチェックが入っているかどうかを調べる方法
		ans[#].checked		return bool
*/
function checkTest#() {
    //ラジオボタンやチェックボックスの数だけの大きさの配列ansが生成される
    var ans = document.FormTagName.InputTagName;
    if ( /*配列内の要素がすべてfalseの時の条件式*/ ) { //チェックが入っていないときの例外処理
        window.confirm("チェックが入っていません。");
    } else {
        //メインの処理
        if ( /*正解の時にチェックが入っているべきパターンの条件式*/ ) {
            window.confirm("正解です。");
            //正解時の動き
        } else {
            window.confirm("不正解です。");
            //不正解時の動き
        }
    }
}

/*
テキストボックスの正誤判定
parm:this.form
必要なid:テキストボックスのname(テキストボックスの数分ある)
テキストボックス内の要素を調べる方法
		ans#.value		return string
*/
function checkTest#(form) {
    //テキストボックスの数だけ変数が宣言される
    var ans# = form.InputTagName;
    //不正解判断用フラグ
    var flag = false;

    if ( /*テキストボックスのどれかに空欄があるの時の条件式*/ ) {
        window.confirm('空欄があります。');
    } else {

        //この判定はテキストボックスの数だけ行われる
        if ( /*判定*/ ) { // 正誤の判定
            ans#.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans#.style.backgroundColor = "#FFB0B0"; // 不正解の色
            flag = true;
        }

        if (flag) { //不正解時の動き
            window.confirm("不正解です。");
            //不正解時の処理
        }

        //正解したとき
        if ( /*判定*/ ) {
            window.confirm("正解です。");
            //正解時の処理
        }
    }
}
