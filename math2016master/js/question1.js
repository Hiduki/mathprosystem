//チェックテスト1
function checkTest1(form) {
    var ans = document.checkTest1_main.check1_main;
    //チェックが入っていないときの例外処理
    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false && ans[3].checked == false) {
        //モーダルポップアップ表示
        $("#modal_id").text("チェックが入っていません。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //正解
        if (ans[0].checked == false && ans[1].checked == true && ans[2].checked == true && ans[3].checked == false) {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#check1_main_showsub").fadeIn();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //補足1表示
            $("#check1_sub1").slideDown();
        }
    }
}

//チェック1補足1わかった
function checkTest1Sub1T() {
    //補足2表示
    $("#check1_sub2").slideDown();
    //補足2までスクロール
    $(".panel").animate({
        scrollTop: $("#check1_sub2").offset().top
    });
}

//チェック1補足1わからない
function checkTest1Sub1F() {
    //補足1-1表示
    $("#check1_sub1_1").fadeIn("fast");
}

//チェック1補足1-1
function checkTest1sub1_1() {
    //補足2表示
    $("#check1_sub2").slideDown();
    //補足2までスクロール
    $(".panel").animate({
        scrollTop: $("#check1_sub2").offset().top
    });

}

//チェック1補足2わかった
function checkTest1Sub2T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト1を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック1補足2わからない
function checkTest1Sub2F() {
    //補足2-1表示
    document.getElementById("check1_sub2_1").style.display = "block";
}

//チェック1補足2-1
function checkTestSub2_1() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト1を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック2-1(1)
function checkTest2_1_1() {
    var ans = document.checkTest2_1_1_main.checkTest2_1_2_main_select;
    if (ans.value == "1") {
        //モーダルポップアップ表示
        $("#modal_id").text("選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "4") { // 正誤の判定
            ans.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
        }
        //正解
        if (ans.value == "4") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //チェック2-1(2)表示
            $("#check2_1_2_main").slideDown();
            //チェック2-1(2)までスクロール
            $(".panel").animate({
                scrollTop: $("#check2_1_2_main").offset().top
            });
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("もう一度チェックテスト1を見てみよう。");
            $('[data-remodal-id=modal]').remodal().open();
            //現在のページを記録
            currentPageNum.num = 2;
            //左のページへスライド
            $.fn.fullpage.moveSlideLeft();
            //現在のページに戻るようボタンを出現させる
            $("#go_to_page").fadeIn("slow");
            //nextボタンを青色に
            $("#next_button_gray").fadeOut();
            $("#next_button").fadeIn("slow");
        }
    }
}

//チェック2-1(2)
function checkTest2_1_2() {
    var ans = document.checkTest2_1_2_main.checkTest2_1_2_main_select;

    if (ans.value == "1") {
        //モーダルポップアップ表示
        $("#modal_id").text("選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "3") { // 正誤の判定
            ans.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
        }
        //正解
        if (ans.value == "3") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#check2_1_2_main_showsub").fadeIn();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //補足1表示
            $("#check2_1_2_sub1").slideDown();
            //補足1までスクロール
            $(".panel").animate({
                scrollTop: $("#check2_1_2_sub1").offset().top
            });
        }
    }
}

//チェック2-1(2)補足
function checkTest2_1_2Sub1() {
    var ans = document.checkTest2_1_2_sub1.check2_1_2_sub1;
    //チェックが入っていないときの例外処理
    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false) {
        //モーダルポップアップ表示
        $("#modal_id").text("1つ選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //メインの処理
        if (ans[1].checked == true) {
            //モーダルポップアップ表示
            $("#modal_id").text("チェックテスト2-1(2)を解きなおしてみよう。");
            $('[data-remodal-id=modal]').remodal().open();
        } else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //補足2表示
            document.getElementById("check2_1_2_sub2").style.display = "block";
            //補足2までスクロール
            //$(".panel").animate({
            //    scrollTop: $("#check2_1_2_sub2").offset().top
            //});
        }
    }
}

//チェック2-1(2)補足2
function checkTest2_1_2Sub2() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト2-1(2)を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
}

//チェック2-2(1)
function checkTest2_2_1() {
    var ans1 = document.checkTest2_2_1_main.checkTest2_2_1_main_select1;
    var ans2 = document.checkTest2_2_1_main.checkTest2_2_1_main_select2;
    if (ans1.value == "1" || ans2.value == "1") {
        var message = "";
        if (ans1.value == "1") {
            message += "ア ";
        }
        if (ans2.value == "1") {
            message += "イ ";
        }
        //モーダルポップアップ表示
        $("#modal_id").text(message + "を選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if ((ans1.value == "2" && ans2.value == "4") || (ans1.value == "4" && ans2.value == "2")) { // 正誤の判定
            ans1.style.backgroundColor = "#00FFFF"; // 正解の色
            ans2.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans1.style.backgroundColor = "#FFB0B0"; // 不正解の色
            ans2.style.backgroundColor = "#FFB0B0"; // 不正解の色
        }
        //正解
        if ((ans1.value == "2" && ans2.value == "4") || (ans1.value == "4" && ans2.value == "2")) {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //2-2(2)表示
            $("#check2_2_2_main").slideDown();
            $(".panel").animate({
                scrollTop: $("#check2_2_2_main").offset().top
            }); //補足までスクロール
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("もう一度チェックテスト1を見てみよう。");
            $('[data-remodal-id=modal]').remodal().open();
            //現在のページを記録
            currentPageNum.num = 3;
            //チェックテスト1へ移動
            $.fn.fullpage.moveTo('page', 1);
            //現在のページに戻るようボタンを出現させる
            $("#go_to_page").fadeIn("slow");
            //nextボタンを青色に
            $("#next_button_gray").fadeOut();
            $("#next_button").fadeIn("slow");
        }
    }
}

//チェック2-2(2)
function checkTest2_2_2(form) {
    var a = document.checkTest2_2_2_main.check2_2_2_main_select1;
    var i = document.checkTest2_2_2_main.check2_2_2_main_select2;

    if (a.value == "1" || i.value == "1") {
        $("#modal_id").text("すべて選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //色関係
        if (a.value == "3") a.style.backgroundColor = "#00FFFF"; // 正解の色
        else a.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (i.value == "4") i.style.backgroundColor = "#00FFFF"; // 正解の色
        else i.style.backgroundColor = "#FFB0B0"; // 不正解の色

        if (a.value == "3" && i.value == "4") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#check2_2_2_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            if (a.value == "2" && i.value == "5") {
                $("#check2_2_2_sub1").slideDown();
                $(".panel").animate({
                    scrollTop: $("#check2_2_2_sub1").offset().top
                }); //補足までスクロール
                $("#modal_id").text("不正解です。");
                $('[data-remodal-id=modal]').remodal().open();
            } else {
                $("#check2_2_2_sub2").slideDown();
                $(".panel").animate({
                    scrollTop: $("#check2_2_2_sub2").offset().top
                }); //補足までスクロール
                $("#modal_id").text("不正解です。");
                $('[data-remodal-id=modal]').remodal().open();
            }
        }
    }
}

function checkTest2_2_2Sub() {
    $("#modal_id").text("もう一度解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    $(".panel").animate({
        scrollTop: 300
    }); //2-2-2までスクロール
}

//チェック3-1
function checkTest3_1(form) {
    var ans = document.checkTest3_1_main.check3_1_main;

    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false && ans[3].checked == false) { //チェックが入っていないときの例外処理
        $("#modal_id").text("チェックが入っていません。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //メインの処理
        if (ans[2].checked == true) {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#check3_1_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            document.getElementById("check3_1_sub").style.display = "block";
            $(".panel").animate({
                scrollTop: $("#check3_1_sub").offset().top
            }); //補足までスクロール
        }
    }
}

//チェック3-1わかったボタン
function checkTest3_1Sub1() {
    $("#modal_id").text("チェックテスト3-1を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    $(".panel").animate({
        scrollTop: 0
    }); //一番上までスクロール
}

//チェック3-2
function checkTest3_2(form) {
    var ans = document.checkTest3_2_main.check3_2_main;

    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false && ans[3].checked == false) { //チェックが入っていないときの例外処理
        $("#modal_id").text("チェックが入っていません。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //メインの処理
        if (ans[3].checked == true) {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#check3_2_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            document.getElementById("check3_2_sub").style.display = "block";
        }
    }
}

//チェック3-2わかったボタン
function checkTest3_2Sub1() {
    $("#modal_id").text("チェックテスト3-2を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
}

//チェック4
function checkTest4(form) {
    var ans = form.check4_main_ans;
    if (ans.value == "") {
        $("#modal_id").text("空欄があります。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "1") {
            ans.style.backgroundColor = "#00FFFF"; // 正解の色
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#check4_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            $("#check4_sub").slideDown();
        }
    }
}

function checkTest4Sub() {
    $("#modal_id").text("チェックテスト4を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
}


/*---------------------------------------*/
/*---------------------------------------*/
/*---------------------------------------*/
/*---------------見せられないよ！----------*/
/*---------------------------------------*/
/*---------------------------------------*/
/*---------------------------------------*/
/*---------------------------------------*/

//本問1
function mainTest1() {
    var a = document.questionTest1_main.questionTest1_main_select1;
    var i = document.questionTest1_main.questionTest1_main_select2;
    var u = document.questionTest1_main.questionTest1_main_select3;
    var e = document.questionTest1_main.questionTest1_main_select4;
    var o = document.questionTest1_main.questionTest1_main_select5;
    var ka = document.questionTest1_main.questionTest1_main_select6;
    var ki = document.questionTest1_main.questionTest1_main_select7;
    var flag = false;

    if (a.value == "1" || i.value == "1" || u.value == "1" || e.value == "1" || o.value == "1" || ka.value == "1" || ki.value == "1") {
        $("#modal_id").text("すべて選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //色関係
        if (a.value == "11") a.style.backgroundColor = "#00FFFF"; // 正解の色
        else a.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (i.value == "2") i.style.backgroundColor = "#00FFFF"; // 正解の色
        else i.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (u.value == "5") u.style.backgroundColor = "#00FFFF"; // 正解の色
        else u.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (e.value == "8") e.style.backgroundColor = "#00FFFF"; // 正解の色
        else e.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (o.value == "9") o.style.backgroundColor = "#00FFFF"; // 正解の色
        else o.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (ka.value == "2") ka.style.backgroundColor = "#00FFFF"; // 正解の色
        else ka.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (ki.value == "2") ki.style.backgroundColor = "#00FFFF"; // 正解の色
        else ki.style.backgroundColor = "#FFB0B0"; // 不正解の色

        if (a.value == "11" && i.value == "2" && u.value == "5" && e.value == "8" && o.value == "9" && ka.value == "2" && ki.value == "2") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            if (a.value == "11" && i.value == "2" && ka.value == "2" && ki.value == "2") {
                currentPageNum.num = 9;
                $.fn.fullpage.moveTo('page', 3);
                $("#go_to_page").fadeIn('slow');
                //nextボタンを青色に
                $("#next_button_gray").fadeOut();
                $("#next_button").fadeIn("slow");
            } else {
                currentPageNum.num = 9;
                $.fn.fullpage.moveTo('page', 4);
                $("#go_to_page").fadeIn('slow');
                //nextボタンを青色に
                $("#next_button_gray").fadeOut();
                $("#next_button").fadeIn("slow");
            }
            $("#modal_id").text("不正解です。 チェックテストを見直しましょう。");
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
}

//本問2
function mainTest2() {
    var a = document.questionTest2_main.questionTest2_main_select1;
    var i = document.questionTest2_main.questionTest2_main_select2;
    var u = document.questionTest2_main.questionTest2_main_select3;
    var e = document.questionTest2_main.questionTest2_main_select4;

    if (a.value == "1" || i.value == "1" || u.value == "1" || e.value == "1") {
        $("#modal_id").text("すべて選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (a.value == "9") a.style.backgroundColor = "#00FFFF"; // 正解の色
        else a.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (i.value == "7") i.style.backgroundColor = "#00FFFF"; // 正解の色
        else i.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (u.value == "2") u.style.backgroundColor = "#00FFFF"; // 正解の色
        else u.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (e.value == "3") e.style.backgroundColor = "#00FFFF"; // 正解の色
        else e.style.backgroundColor = "#FFB0B0"; // 不正解の色

        if (a.value == "9" && i.value == "7" && u.value == "2" && e.value == "3") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            if (a.value == "9") {
                $("#modal_id").text("もう1度解きなおしてみよう！");
                $('[data-remodal-id=modal]').remodal().open();
            } else {
                $("#modal_id").text("不正解です。チェックテストを見直した後、もう1度解きなおしてみよう！");
                $('[data-remodal-id=modal]').remodal().open();
                currentPageNum.num = 10;
                $.fn.fullpage.moveTo('page', 5);
                $("#go_to_page").fadeIn('slow');
            }
        }
    }
}

//本問3
function mainTest3(form) {
    ans = form.question3_main_ans;
    if (ans.value == "") {
        $("#modal_id").text("空欄があります。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "1") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            $("#modal_id").text("不正解です。 前の問題を見直そう。");
            $('[data-remodal-id=modal]').remodal().open();
            currentPageNum.num = 11;
            $.fn.fullpage.moveTo('page', 10);
            $("#go_to_page").fadeIn('slow');
        }
    }
}

//本問4
function mainTest4() {
    var ans = document.questionTest4_main.questionTest4_main_select1;

    if (ans.value == "1") {
        $("#modal_id").text("選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "4") ans.style.backgroundColor = "#00FFFF"; // 正解の色
        else ans.style.backgroundColor = "#FFB0B0"; // 不正解の色

        if (ans.value == "4") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange1();
            $("#question4_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else if (ans.value == "2") {
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            $("#question4_sub1").slideDown();
            $(".panel").animate({
                scrollTop: 10000
            }); //上までスクロール
        } else {
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            $("#question4_sub2").slideDown();
            $(".panel").animate({
                scrollTop: 10000
            }); //上までスクロール
        }
    }
}

function mainTest4Sub() {
    $("#modal_id").text("もう1度挑戦してみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    $(".panel").animate({
        scrollTop: 0
    }); //上までスクロール
}


//補足全表示ボタン
function showSubAllChk1(){
	$("#check1_sub1").fadeIn();
	$("#check1_sub1_1").fadeIn();
	$("#check1_sub2").fadeIn();
	$("#check1_sub2_1").fadeIn();
}

function showSubAllChk2_1(){
	$("#check2_1_2_sub1").fadeIn();
	$("#check2_1_2_sub2").fadeIn();
}

function showSubAllChk2_2(){
	$("#check2_2_2_sub1").fadeIn();
	$("#check2_2_2_sub2").fadeIn();
}

function showSubAllChk3_1(){
	$("#check3_1_sub").fadeIn();
}

function showSubAllChk3_2(){
	$("#check3_2_sub").fadeIn();
}

function showSubAllChk4(){
	$("#check4_sub").fadeIn();
}
