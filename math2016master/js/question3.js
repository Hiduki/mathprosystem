//チェック1
function checkTest1() {
    var ans = document.checkTest1_main.check1_main;

    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false && ans[3].checked == false) {
        $("#modal_id").text("チェック入っていないよ。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans[0].checked == true && ans[1].checked == true && ans[2].checked == true && ans[3].checked == false) {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange3();
			$("#check1_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            $("#check1_sub1").slideDown();
        }
    }
}

//チェック1補足
function checkTest1Sub1() {
    var ans = document.checkTest1_sub1.check1_sub;
    if (ans[0].checked && ans[1].checked && ans[2].checked && ans[3].checked) {
        $("#modal_id").text("チェックテスト1を解きなおしてみよう。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        $("#modal_id").text("確認しましょう。");
        $('[data-remodal-id=modal]').remodal().open();
    }
}

//チェック2-1用カウンター
var check2_1Count = {}
check2_1Count.num = 0;

//チェック2-1
function checkTest2_1(form) {
    var ans = form.check2_1_main_ans;
    if (ans.value == "") {
        $("#modal_id").text("空欄があります。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "1") {
            ans.style.backgroundColor = "#00FFFF"; // 正解の色
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange3();
			$("#check2_1_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            if (check2_1Count.num == 0) {
                $("#check2_1_sub1").slideDown();
                $(".panel").animate({
                    scrollTop: $("#check2_1_sub1").offset().top
                }); //
            } else if (check2_1Count.num == 1) {
                $("#check2_1_sub2").slideDown();
                $(".panel").animate({
                    scrollTop: $("#check2_1_sub2").offset().top
                });
            }
        }
    }
}

function checkTest2_1Sub1() {
    $("#modal_id").text("チェックテスト2-1を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    if (check2_1Count.num == 0) check2_1Count.num++;
    $(".panel").animate({
        scrollTop: 0
    }); //上までスクロール
}

function checkTest2_1Sub2() {
    $("#modal_id").text("チェックテスト2-1を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    if (check2_1Count.num == 1) check2_1Count.num++;
    $(".panel").animate({
        scrollTop: 0
    }); //上までスクロール
}

//チェック2-2用カウンター
var check2_2Count = {}
check2_2Count.num = 0;

//チェック2-2
function checkTest2_2(form) {
    var ans = form.check2_2_main_ans;
    if (ans.value == "") {
        $("#modal_id").text("空欄があります。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (check2_2Count.num == 0) {
            if (ans.value == "0") {
                ans.style.backgroundColor = "#00FFFF"; // 正解の色
                $("#modal_id").text("正解です。");
                $('[data-remodal-id=modal]').remodal().open();
                flagChange3();
				$("#check2_2_main_showsub").fadeIn();
                //$.fn.fullpage.moveSlideRight(); //右のページへスライド
            } else {
                ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
                $("#modal_id").text("不正解です。");
                $('[data-remodal-id=modal]').remodal().open();
                $("#check2_2_sub1").slideDown();
            }
        } else if (check2_2Count.num == 1) {
            if (ans.value == "0") {
                ans.style.backgroundColor = "#00FFFF"; // 正解の色
                $("#modal_id").text("正解です。");
                $('[data-remodal-id=modal]').remodal().open();
                flagChange3();
                //$.fn.fullpage.moveSlideRight(); //右のページへスライド
            } else {
                ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
                $("#modal_id").text("不正解です。");
                $('[data-remodal-id=modal]').remodal().open();
                $("#check2_2_sub5").slideDown();
                $(".panel").animate({
                    scrollTop: $("#check2_2_sub5").offset().top
                }); //補足5までスクロール
            }
        } else {
            if (ans.value == "0") {
                ans.style.backgroundColor = "#00FFFF"; // 正解の色
                $("#modal_id").text("正解です。");
                $('[data-remodal-id=modal]').remodal().open();
                flagChange3();
                //$.fn.fullpage.moveSlideRight(); //右のページへスライド
            } else {
                ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
                $("#modal_id").text("不正解です。 補足を見直そう。");
                $('[data-remodal-id=modal]').remodal().open();
            }
        }
    }
}

//チェック2-2補足1わかった
function checkTest2_2Sub1T() {
    $("#modal_id").text("チェックテスト2-2を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    if (check2_2Count.num == 0) check2_2Count.num++;
}

//チェック2-2補足1わからない
function checkTest2_2Sub1F() {
    $("#check2_2_sub2").slideDown();
    $(".panel").animate({
        scrollTop: $("#check2_2_sub2").offset().top
    }); //補足2までスクロール
}

//チェック2-2補足2
function checkTest2_2Sub2() {
    $("#check2_2_sub3").slideDown();
    $(".panel").animate({
        scrollTop: $("#check2_2_sub3").offset().top
    }); //補足3までスクロール
}

//チェック2-2補足3
function checkTest2_2Sub3(form) {
    var ans = form.check2_2_sub3_ans;
    if (ans.value == "") {
        $("#modal_id").text("空欄があります。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "0") {
            ans.style.backgroundColor = "#00FFFF"; // 正解の色
            $("#modal_id").text("正解です。チェックテスト2-2を解きなおしてみよう。");
            $('[data-remodal-id=modal]').remodal().open();
            if (check2_2Count.num == 0) check2_2Count.num++;
            $(".panel").animate({
                scrollTop: 0
            }); //上までスクロール
        } else {
            ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            $("#check2_2_sub4").slideDown();
            $(".panel").animate({
                scrollTop: $("#check2_2_sub4").offset().top
            }); //補足4までスクロール
        }
    }
}

//チェック2-2補足4
function checkTest2_2Sub4() {
    $("#modal_id").text("補足3を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //if (check2_2Count.num == 0) check2_2Count.num++;
    $(".panel").animate({
        scrollTop: 400
    }); //補足3までスクロール
}

//チェック2-2補足5
function checkTest2_2Sub5(form) {
    var ans = document.checkTest2_2_sub5.checkTest2_2_main_select;
    if (ans.value == "1") {
        $("#modal_id").text("選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans.value == "3") {
            ans.style.backgroundColor = "#00FFFF"; // 正解の色
            $("#modal_id").text("正解です。チェックテスト2-2を解きなおしてみよう。");
            $('[data-remodal-id=modal]').remodal().open();
            if (check2_2Count.num == 1) check2_2Count.num++;
            $(".panel").animate({
                scrollTop: 0
            }); //一番上までスクロール
        } else {
            ans.style.backgroundColor = "#FFB0B0"; // 不正解の色
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            $("#check2_2_sub6").slideDown();
            $(".panel").animate({
                scrollTop: 100000
            }); //補足6までスクロール
        }
    }
}

//チェック2-2補足6
function checkTest2_2Sub6() {
    $("#modal_id").text("補足5を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //if (check2_2Count.num == 1) check2_2Count.num++;
}

//本問題1
function mainTest1() {
    var a = document.questionTest1_main.questionTest1_main_select1;
    var i = document.questionTest1_main.questionTest1_main_select2;
    var u = document.questionTest1_main.questionTest1_main_select3;
    var e = document.questionTest1_main.questionTest1_main_select4;
    var o = document.questionTest1_main.questionTest1_main_select5;
    var ka = document.questionTest1_main.questionTest1_main_select6;
    var ki = document.questionTest1_main.questionTest1_main_select7;
    var flag = false;

    if (a.value == "1" || i.value == "1" || u.value == "1" || e.value == "1" || o.value == "1" || ka.value == "1" || ki.value == "1") {
        $("#modal_id").text("すべて選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //色関係
        if (a.value == "7") a.style.backgroundColor = "#00FFFF"; // 正解の色
        else a.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (i.value == "2") i.style.backgroundColor = "#00FFFF"; // 正解の色
        else i.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (u.value == "6") u.style.backgroundColor = "#00FFFF"; // 正解の色
        else u.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (e.value == "4") e.style.backgroundColor = "#00FFFF"; // 正解の色
        else e.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (o.value == "8") o.style.backgroundColor = "#00FFFF"; // 正解の色
        else o.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (ka.value == "3") ka.style.backgroundColor = "#00FFFF"; // 正解の色
        else ka.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (ki.value == "5") ki.style.backgroundColor = "#00FFFF"; // 正解の色
        else ki.style.backgroundColor = "#FFB0B0"; // 不正解の色

        if (a.value == "7" && i.value == "2" && u.value == "6" && e.value == "4" && o.value == "8" && ka.value == "3" && ki.value == "5") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange3();
			$("#question1_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            $("#question1_sub").slideDown();
            $(".panel").animate({
                scrollTop: $("#question1_sub").offset().top
            }); //補足までスクロール
            if (a.value != "7") {
                $("#question1_sub1").slideDown();
            }
            if (i.value != "2") {
                currentPageNum.num = 6;
                $.fn.fullpage.moveTo('page', 1);
                $("#go_to_page").fadeIn('slow');
                flag = true;
                //nextボタンを青色に
                $("#next_button_gray").fadeOut();
                $("#next_button").fadeIn("slow");
            }
            if (u.value != "6") {
                $("#question1_sub2").slideDown();
            }
            if (!(e.value == "4" && o.value == "8" && ka.value == "3" && ki.value == "5")) {
                $("#question1_sub3").slideDown();
            }

            if (flag) {
                $("#modal_id").text("不正解です。 チェックテストを見直した後、補足をチェックしましょう。");
            } else {
                $("#modal_id").text("不正解です。 補足をチェックしましょう。");
            }
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
}

//本問題2
function mainTest2() {
    var a = document.questionTest2_main.question2_main_select1;
    var i = document.questionTest2_main.question2_main_select2;

    if (a.value == "1" || i.value == "1") {
        $("#modal_id").text("すべて選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        //色関係
        if (a.value == "5") a.style.backgroundColor = "#00FFFF"; // 正解の色
        else a.style.backgroundColor = "#FFB0B0"; // 不正解の色
        if (i.value == "2") i.style.backgroundColor = "#00FFFF"; // 正解の色
        else i.style.backgroundColor = "#FFB0B0"; // 不正解の色

        if (a.value == "5" && i.value == "2") {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange3();
			$("#question2_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight(); //右のページへスライド
        } else {
            $("#question2_sub").slideDown();
            $(".panel").animate({
                scrollTop: $("#question2_sub").offset().top
            }); //補足までスクロール

            if (a.value != "5") {
                $("#question2_sub1").slideDown();
            }

            if (i.value != "2") {
                $("#question2_sub2").slideDown();
            }
            $("#modal_id").text("不正解です。 補足をチェックしましょう。");
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
}

function mainTest2Sub() {
    $("#modal_id").text("もう1度挑戦してみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    $(".panel").animate({
        scrollTop: 0
    }); //上までスクロール
}

//本問題3
function mainTest3(form) {
    var ans1 = document.questionTest3_main.question3_main_select1;
    var ans2 = form.question3_main_ans2;
    var ans3 = form.question3_main_ans3;
    var flag = false;

    if (ans1.value == "1" || ans2.value == "" || ans3.value == "") {
        $("#modal_id").text("空欄があります。");
        $('[data-remodal-id=modal]').remodal().open();
    } else {
        if (ans1.value == "3") { // 正誤の判定
            ans1.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans1.style.backgroundColor = "#FFB0B0"; // 不正解の色
            flag = true;
        }

        if (ans2.value == "0") {
            ans2.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans2.style.backgroundColor = "#FFB0B0"; // 不正解の色
            flag = true;
        }

        if (ans3.value == "1") {
            ans3.style.backgroundColor = "#00FFFF"; // 正解の色
        } else {
            ans3.style.backgroundColor = "#FFB0B0"; // 不正解の色
            flag = true;
        }

        if (flag) { //不正解時の動き
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
			$("#question3_sub").slideDown();
			$(".panel").animate({
				scrollTop: $("#question3_sub").offset().top
			});
        } else {
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange3();
			$("#question3_main_showsub").fadeIn();
            //$.fn.fullpage.moveSlideRight();
        }
    }
}

//本問4
function mainTest4(){
	var ans = document.questionTest4_main.questionTest4_main_select;
    //空欄があるとき
    if (ans.value == "1") {
        if (ans.value == "1") {
            //モーダルポップアップ表示
            $("#modal_id").text("選択してください。");
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
    //空欄がないとき
    else {
        //色つけ
        if (ans.value == "4") {
            ans.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        //正解
        if (ans.value == "4") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange3();
            $("#question4_main_showsub").fadeIn();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            if (ans.value == "2") {
                //補足1表示
                $("#question4_sub1").slideDown();
                //補足1までスクロール
                $(".panel").animate({
                    scrollTop: $("#question4_sub1").offset().top
                });
            } else {
                //補足2表示
                $("#question4_sub2").slideDown();
                //補足2までスクロール
                $(".panel").animate({
                    scrollTop: $("#question4_sub2").offset().top
                });
            }
        }
    }
}

function question4Sub() {
    //モーダルポップアップ表示
    $("#modal_id").text("もう一度解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

function showSubAllChk1(){
	$("#check1_sub1").fadeIn();
}

function showSubAllChk2_1(){
	$("#check2_1_sub1").fadeIn();
	$("#check2_1_sub2").fadeIn();
}

function showSubAllChk2_2(){
	$("#check2_2_sub1").fadeIn();
	$("#check2_2_sub2").fadeIn();
	$("#check2_2_sub3").fadeIn();
	$("#check2_2_sub4").fadeIn();
	$("#check2_2_sub5").fadeIn();
	$("#check2_2_sub6").fadeIn();
}

function showSubAllMain1(){
	$("#question1_sub1").fadeIn();
	$("#question1_sub2").fadeIn();
	$("#question1_sub3").fadeIn();
}

function showSubAllMain2(){
	$("#question2_sub").fadeIn();
	$("#question2_sub2").fadeIn();
}

function showSubAllMain3(){
	$("#question3_sub").fadeIn();
}

function showSubAllMain4(){
	$("#question4_sub1").fadeIn();
	$("#question4_sub2").fadeIn();
}
