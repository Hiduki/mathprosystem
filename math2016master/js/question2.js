//チェックテスト1
function checkTest1() {
    var ans = document.checkTest1_main.check1_main;
    //チェックが入っていないとき
    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false && ans[3].checked == false) {
        //モーダルポップアップ表示
        $("#modal_id").text("1つ選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //チェックが入っているとき
    else {
        //正解
        if (ans[2].checked) {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange2();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //補足表示
            $("#check1_supplementation").slideDown();
            //補足までスクロール
            $(".panel").animate({
                scrollTop: $("#check1_supplementation").offset().top
            });
        }
    }
}

//チェック1補足
function checkTest1Sub1() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト1を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェックテスト2
function checkTest2() {
    var ans1 = document.checkTest2_main.checkTest2_main_select1;
    var ans2 = document.checkTest2_main.checkTest2_main_select2;
    var ans3 = document.checkTest2_main.checkTest2_main_select3;
    var ans4 = document.checkTest2_main.checkTest2_main_select4;
    //空欄があるとき
    if (ans1.value == "1" || ans2.value == "1" || ans3.value == "1" || ans4.value == "1") {
        var message = "";
        if (ans1.value == "1") {
            message += "ア ";
        }
        if (ans2.value == "1") {
            message += "イ ";
        }
        if (ans3.value == "1") {
            message += "ウ ";
        }
        if (ans4.value == "1") {
            message += "エ ";
        }
        //モーダルポップアップ表示
        $("#modal_id").text(message + "を選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //空欄がないとき
    else {
        //色つけ
        if (ans1.value == "4") {
            ans1.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans1.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans2.value == "4") {
            ans2.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans2.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans3.value == "2") {
            ans3.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans3.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans4.value == "4") {
            ans4.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans4.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        //正解
        if (ans1.value == "4" && ans2.value == "4" && ans3.value == "2" && ans4.value == "4") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange2();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //補足1表示
            $("#check2_supplementation1").slideDown();
            //補足1までスクロール
            $(".panel").animate({
                scrollTop: $("#check2_supplementation1").offset().top
            });
        }
    }
}

//チェック2補足1わかった
function checkTest2Sub1T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト2を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック2補足1わからない
function checkTest2Sub1F() {
    //補足2表示
    $("#check2_supplementation2").slideDown();
    //補足2までスクロール
    $(".panel").animate({
        scrollTop: $("#check2_supplementation2").offset().top
    });
}

//チェック2補足2わかった
function checkTest2Sub2T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト2を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック2補足2わからない
function checkTest2Sub2F() {
    //補足3表示
    $("#check2_supplementation3").slideDown();
    //補足3までスクロール
    $(".panel").animate({
        scrollTop: $("#check2_supplementation3").offset().top
    });
}

//チェック2補足3わかった
function checkTest2Sub3T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト2を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック2補足3このまま進む
function checkTest2Sub3F() {
    flagChange2();
    //右のページへスライド
    $.fn.fullpage.moveSlideRight();
}

//チェック3
function checkTest3() {
    var ans = document.checkTest3_main.check3_main;
    //チェックが入っていないとき
    if (ans[0].checked == false && ans[1].checked == false && ans[2].checked == false && ans[3].checked == false) {
        //モーダルポップアップ表示
        $("#modal_id").text("1つ選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //チェックが入っているとき
    else {
        //正解
        if (ans[2].checked) {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange2();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            //補足1表示
            $("#check3_supplementation1").slideDown();
            //補足1までスクロール
            $(".panel").animate({
                scrollTop: $("#check3_supplementation1").offset().top
            });
        }
    }
}

//チェック3補足1わかった
function checkTest3Sub1T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト3を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック3補足1わからない
function checkTest3Sub1F() {
    //補足2表示
    $("#check3_supplementation2").slideDown();
    //補足2までスクロール
    $(".panel").animate({
        scrollTop: $("#check3_supplementation2").offset().top
    });
}

//チェック3補足2わかった
function checkTest3Sub2T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト3を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//チェック3補足2わからない
function checkTest3Sub2F() {
    //補足3表示
    $("#check3_supplementation3").slideDown();
    //補足3までスクロール
    $(".panel").animate({
        scrollTop: $("#check3_supplementation3").offset().top
    });
}

//チェック3補足3わかった
function checkTest3Sub3T() {
    //モーダルポップアップ表示
    $("#modal_id").text("チェックテスト3を解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//本問1
function mainTest1() {
    var ans1 = document.questionTest1_main.questionTest1_main_select1;
    var ans2 = document.questionTest1_main.questionTest1_main_select2;
    var ans3 = document.questionTest1_main.questionTest1_main_select3;
    var ans4 = document.questionTest1_main.questionTest1_main_select4;
    //空欄があるとき
    if (ans1.value == "1" || ans2.value == "1" || ans3.value == "1" || ans4.value == "1") {
        var message = "";
        if (ans1.value == "1") {
            message += "ア ";
        }
        if (ans2.value == "1") {
            message += "イ ";
        }
        if (ans3.value == "1") {
            message += "ウ ";
        }
        if (ans4.value == "1") {
            message += "エ ";
        }
        //モーダルポップアップ表示
        $("#modal_id").text(message + "を選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //空欄がないとき
    else {
        //色つけ
        if (ans1.value == "5") {
            ans1.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans1.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans2.value == "2") {
            ans2.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans2.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans3.value == "4") {
            ans3.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans3.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans4.value == "7") {
            ans4.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans4.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        //正解
        if (ans1.value == "5" && ans2.value == "2" && ans3.value == "4" && ans4.value == "7") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange2();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            var message = "不正解です。";
            if (ans1.value != "5") {
                message += "チェックテスト1を確認しよう。";
                //チェックテスト1へ戻す
                $.fn.fullpage.moveTo('page', 1);
                $("#go_to_page").fadeIn("slow");
                currentPageNum.num = 6;
                //nextボタンを青色に
                $("#next_button_gray").fadeOut();
                $("#next_button").fadeIn("slow");
            } else {
                if (ans2.value == "2" && ans3.value == "4") {
                    message += "チェックテスト1を確認しよう。";
                    //チェックテスト1へ戻す
                    $.fn.fullpage.moveTo('page', 1);
                    $("#go_to_page").fadeIn("slow");
                    currentPageNum.num = 6;
                    //nextボタンを青色に
                    $("#next_button_gray").fadeOut();
                    $("#next_button").fadeIn("slow");
                } else {
                    //メッセージは補足と重複するので削除
                    //補足表示
                    $("#question1_supplementation").slideDown();
                    //補足までスクロール
                    $(".panel").animate({
                        scrollTop: $("#question1_supplementation").offset().top
                    });
                }
            }
            //モーダルポップアップ表示
            $("#modal_id").text(message);
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
}

//本問1補足
function mainTest1Sub() {
    //モーダルポップアップ表示
    $("#modal_id").text("もう一度解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//本問2
function mainTest2() {
    var ans1 = document.questionTest2_main.questionTest2_main_select1;
    var ans2 = document.questionTest2_main.questionTest2_main_select2;
    var ans3 = document.questionTest2_main.questionTest2_main_select3;
    var ans4 = document.questionTest2_main.questionTest2_main_select4;
    //空欄があるとき
    if (ans1.value == "1" || ans2.value == "1" || ans3.value == "1" || ans4.value == "1") {
        var message = "";
        if (ans1.value == "1") {
            message += "ア ";
        }
        if (ans2.value == "1") {
            message += "イ ";
        }
        if (ans3.value == "1") {
            message += "ウ ";
        }
        if (ans4.value == "1") {
            message += "エ ";
        }
        //モーダルポップアップ表示
        $("#modal_id").text(message + "を選択してください。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //空欄がないとき
    else {
        //色つけ
        if (ans1.value == "7") {
            ans1.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans1.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans2.value == "9") {
            ans2.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans2.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans3.value == "3") {
            ans3.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans3.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        if (ans4.value == "4") {
            ans4.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans4.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        //正解
        if (ans1.value == "7" && ans2.value == "9" && ans3.value == "3" && ans4.value == "4") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange2();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        //TODO: 補足への分岐を追加する
        else {
            var message = "不正解です。";
            if (ans1.value != "7") {
                message += "チェックテスト2を確認しよう。";
                //チェックテスト2へ戻す
                $.fn.fullpage.moveTo('page', 2);
                $("#go_to_page").fadeIn("slow");
                currentPageNum.num = 7;
                //nextボタンを青色に
                $("#next_button_gray").fadeOut();
                $("#next_button").fadeIn("slow");
            } else {
                if (ans2.value != "9") {
                    message += "チェックテスト3を確認しよう。";
                    //チェックテスト3へ戻す
                    $.fn.fullpage.moveTo('page', 3);
                    $("#go_to_page").fadeIn("slow");
                    currentPageNum.num = 7;
                    //nextボタンを青色に
                    $("#next_button_gray").fadeOut();
                    $("#next_button").fadeIn("slow");
                } else {
                    //補足表示
                    $("#question2_supplementation").slideDown();
                    //補足までスクロール
                    $(".panel").animate({
                        scrollTop: $("#question2_supplementation").offset().top
                    });
                }
            }
            //モーダルポップアップ表示
            $("#modal_id").text(message);
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
}

//本問2補足
function mainTest2Sub() {
    //モーダルポップアップ表示
    $("#modal_id").text("もう一度解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//本問題3
function mainTest3() {
    var ans = document.questionTest3_main.questionTest3_main_select;
    //空欄があるとき
    if (ans.value == "1") {
        if (ans.value == "1") {
            //モーダルポップアップ表示
            $("#modal_id").text("選択してください。");
            $('[data-remodal-id=modal]').remodal().open();
        }
    }
    //空欄がないとき
    else {
        //色つけ
        if (ans.value == "4") {
            ans.style.backgroundColor = "#00FFFF"; //正解の色
        } else {
            ans.style.backgroundColor = "#FFB0B0"; //不正解の色
        }
        //正解
        if (ans.value == "4") {
            //モーダルポップアップ表示
            $("#modal_id").text("正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            flagChange2();
            //右のページへスライド
            //$.fn.fullpage.moveSlideRight();
        }
        //不正解
        else {
            //モーダルポップアップ表示
            $("#modal_id").text("不正解です。");
            $('[data-remodal-id=modal]').remodal().open();
            if (ans.value == "2") {
                //補足1表示
                $("#question3_supplementation1").slideDown();
                //補足1までスクロール
                $(".panel").animate({
                    scrollTop: $("#question3_supplementation1").offset().top
                });
            } else {
                //補足2表示
                $("#question3_supplementation2").slideDown();
                //補足2までスクロール
                $(".panel").animate({
                    scrollTop: $("#question3_supplementation2").offset().top
                });
            }
        }
    }
}

//本問3補足1
function question3Sub1() {
    //モーダルポップアップ表示
    $("#modal_id").text("もう一度解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}

//本問3補足2
function question3Sub2() {
    //モーダルポップアップ表示
    $("#modal_id").text("もう一度解きなおしてみよう。");
    $('[data-remodal-id=modal]').remodal().open();
    //一番上までスクロール
    $(".panel").animate({
        scrollTop: 0
    });
}
