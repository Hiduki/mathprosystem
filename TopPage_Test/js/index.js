window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
        t._e.push(f);
    };

    return t;
}(document, "script", "twitter-wjs"));

twttr.widgets.createTimeline(
    "600720083413962752",
    document.getElementById("container"), {
        height: 400,
        chrome: "nofooter",
        linkColor: "#820bbb",
        borderColor: "#a80000"
    }
);

function goToTopPage() {
    $.fn.fullpage.moveTo("firstPage", 0);
}

function goToQuestionSelectPage() {
    $.fn.fullpage.moveTo("secondPage", 0);
}

function goToUsegePage() {
    $.fn.fullpage.moveTo("3rdPage", 0);
}

function goToQuestionListPage(slideNum) {
    $.fn.fullpage.moveTo("secondPage", slideNum);
}

/*
$(function() {
    $(".imgButton").hover(function() {
        $("img", this).css({
            outline: "5px rgba(8, 121, 255, 0.7) solid"
        });
    }, function() {
        $("img", this).css({
            outline: "0"
        });
    });
});*/

$(function() {
    $('#prevImageEvent').hover(function() {
        $(this).attr('src', $(this).attr('src').replace('_non', '_over'));
    }, function() {
        if (!$(this).hasClass('currentPage')) {
            $(this).attr('src', $(this).attr('src').replace('_over', '_non'));
        }
    });

});
