//右のページにスライドさせるボタン用
function moveRight() {
    $.fn.fullpage.moveSlideRight(); //右のページへスライド
}

//**************
//moveRightの関数たち
//**************
function moveRight1() {
    //現在のスライドの数字を取得
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    //次のスライドが未正解である場合は灰色ボタン
    if (pageFlag1.flags[pageNum] == 0) {
        $("#next_button").fadeOut();
        $("#next_button_gray").fadeIn("slow");
    }
    //prevボタンの表示
    $("#prev_button_gray").fadeOut();
    $("#prev_button").fadeIn("slow");
    $.fn.fullpage.moveSlideRight(); //右のページへスライド
}

function moveRight2() {
    //現在のスライドの数字を取得
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    //次のスライドが未正解である場合は灰色ボタン
    if (pageFlag2.flags[pageNum] == 0) {
        $("#next_button").fadeOut();
        $("#next_button_gray").fadeIn("slow");
    }
    //prevボタンの表示
    $("#prev_button_gray").fadeOut();
    $("#prev_button").fadeIn("slow");
    $.fn.fullpage.moveSlideRight(); //右のページへスライド
}

function moveRight3() {
    //現在のスライドの数字を取得
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    //次のスライドが未正解である場合は灰色ボタン
    if (pageFlag3.flags[pageNum] == 0) {
        $("#next_button").fadeOut();
        $("#next_button_gray").fadeIn("slow");
    }
    //prevボタンの表示
    $("#prev_button_gray").fadeOut();
    $("#prev_button").fadeIn("slow");
    $.fn.fullpage.moveSlideRight(); //右のページへスライド
}

//CurrentPageの名前空間
var currentPageNum = {}
currentPageNum.num = 0;

//CurrentPageに戻る用
function moveCurrentPage() {
    $.fn.fullpage.moveTo('page', currentPageNum.num);
    $("#go_to_page").fadeOut("slow"); //現在のページに戻るようボタンをfadeout
    //nextボタンを灰色に
    $("#next_button").fadeOut();
    $("#next_button_gray").fadeIn("slow");
}


//**************
//ページ移動用のflagたち
//**************
var pageFlag1 = {}
pageFlag1.flags = [1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1];

var pageFlag2 = {}
pageFlag2.flags = [1, 0, 0, 0, 1, 1, 0, 0, 0, 1];

var pageFlag3 = {}
pageFlag3.flags = [1, 0, 0, 0, 1, 1, 0, 0, 0, 1];

//**************
//flag changeの関数たち
//**************

function flagChange1() {
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    pageFlag1.flags[pageNum - 1] = 1;
    $("#next_button_gray").fadeOut();
    $("#next_button").fadeIn("slow");
}

function flagChange2() {
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    pageFlag2.flags[pageNum - 1] = 1;
    $("#next_button_gray").fadeOut();
    $("#next_button").fadeIn("slow");
}

function flagChange3() {
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    pageFlag3.flags[pageNum - 1] = 1;
    $("#next_button_gray").fadeOut();
    $("#next_button").fadeIn("slow");
}

//**************
//next buttonの関数たち
//**************
function nextButton1() {
    //現在のスライドの数字を取得
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    //次のスライドが未正解である場合は灰色ボタン
    if (pageFlag1.flags[pageNum] == 0) {
        $("#next_button").fadeOut();
        $("#next_button_gray").fadeIn("slow");
    }
    //次進むかどうかの判定
    if (pageFlag1.flags[pageNum - 1] == 1) {
        $.fn.fullpage.moveSlideRight();
    } else {
        $("#modal_id").text("まだ先に進めません。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //prevボタンの表示
    $("#prev_button_gray").fadeOut();
    $("#prev_button").fadeIn("slow");
    $("#go_to_page").fadeOut("slow"); //現在のページに戻るようボタンをfadeout
}

function nextButton2() {
    //現在のスライドの数字を取得
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    //次のスライドが未正解である場合は灰色ボタン
    if (pageFlag2.flags[pageNum] == 0) {
        $("#next_button").fadeOut();
        $("#next_button_gray").fadeIn("slow");
    }
    //次進むかどうかの判定
    if (pageFlag2.flags[pageNum - 1] == 1) {
        $.fn.fullpage.moveSlideRight();
    } else {
        $("#modal_id").text("まだ先に進めません。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //prevボタンの表示
    $("#prev_button_gray").fadeOut();
    $("#prev_button").fadeIn("slow");
    $("#go_to_page").fadeOut("slow"); //現在のページに戻るようボタンをfadeout
}

function nextButton3() {
    //現在のスライドの数字を取得
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    //次のスライドが未正解である場合は灰色ボタン
    if (pageFlag3.flags[pageNum] == 0) {
        $("#next_button").fadeOut();
        $("#next_button_gray").fadeIn("slow");
    }
    //次進むかどうかの判定
    if (pageFlag3.flags[pageNum - 1] == 1) {
        $.fn.fullpage.moveSlideRight();
    } else {
        $("#modal_id").text("まだ先に進めません。");
        $('[data-remodal-id=modal]').remodal().open();
    }
    //prevボタンの表示
    $("#prev_button_gray").fadeOut();
    $("#prev_button").fadeIn("slow");
    $("#go_to_page").fadeOut("slow"); //現在のページに戻るようボタンをfadeout
}

//**************
//prev buttonの関数
//**************
function prevButton() {
    var url = window.location.href;
    var pageNum = url.substr(url.length - 2).replace("e", "");
    if (pageNum == "ml" || pageNum == "g") {
        pageNum = 1;
    }
    if (pageNum != "1") {
        $.fn.fullpage.moveSlideLeft();
    }
    //戻った時に左端になるときに戻るボタンを灰色に
    if (pageNum - 1 == "1") {
        $("#prev_button").fadeOut();
        $("#prev_button_gray").fadeIn("slow");
    }
    //nextボタンの表示
    $("#next_button_gray").fadeOut();
    $("#next_button").fadeIn("slow");
    $("#go_to_page").fadeOut("slow"); //現在のページに戻るようボタンをfadeout
}

//**************
//マウスオーバーのイベント処理たち
//**************
$(function() {
    $('#nextImageEvent').hover(function() {
        $(this).attr('src', $(this).attr('src').replace('_arrow', '_arrow_over'));
    }, function() {
        if (!$(this).hasClass('currentPage')) {
            $(this).attr('src', $(this).attr('src').replace('_arrow_over', '_arrow'));
        }
    });

});

$(function() {
    $('#prevImageEvent').hover(function() {
        $(this).attr('src', $(this).attr('src').replace('_arrow', '_arrow_over'));
    }, function() {
        if (!$(this).hasClass('currentPage')) {
            $(this).attr('src', $(this).attr('src').replace('_arrow_over', '_arrow'));
        }
    });

});

$(function() {
    $(".button").hover(function() {
        $(this).animate({
            color: "rgb(26, 173, 165)"
        }, 150);
    }, function() {
        $(this).animate({
            color: "#000000"
        }, 150);
    });
});

$(function() {
    $(".select_button").hover(function() {
        //if ($(".math", this)) {
            $(".math", this).animate({
                backgroundColor: "rgba(190, 237, 202, 0.5)"
            }, 100);
        //} else {
            $(this).animate({
                backgroundColor: "rgba(190, 237, 202, 0.5)"
            }, 100);
        //}
        $("img", this).css({
            outline: "5px rgba(190, 237, 202, 0.5) solid"
        });
    }, function() {
        $(".math", this).animate({
            backgroundColor: "rgba(255, 255, 255, 0)"
        }, 100);
		$(this).animate({
			backgroundColor: "rgba(255, 255, 255, 0)"
		}, 100);
        $("img", this).css({
            outline: "0"
        });
    });
});

/*
$(function() {
    $("#next_button").hover(
        function(e) {
            $("#nextImageEvent").fadeOut();
            $("#next_button_over").fadeIn("slow");
        },
        function(e) {
            $("#next_button_over").fadeOut();
            $("#nextImageEvent").fadeIn("slow");
        }
    );
});
*/
